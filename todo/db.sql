create database todo;
use todo;
create table task (
    id int auto_increment primary key,
    description varchar(255) not null,
    done boolean default false,
    added timestamp default current_timestamp
);